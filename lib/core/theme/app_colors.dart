import 'package:flutter/material.dart';

class AppColors {
  static const Color white = Color(0xffFFFFFF);
  static const Color blue = Color(0xff4631D2);
  static const Color red = Color(0xffEC3A4D);
  static const Color black = Color(0xff000000);
  static const Color grey = Color(0xff929292);
  static const Color background = Color(0xffF3F4F6);
}
