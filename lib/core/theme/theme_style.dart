import 'package:flutter/material.dart';
import 'package:test_codeunion/core/theme/app_colors.dart';

class AppTheme {
  static const String manrope = 'Manrope';

  static ThemeData defaultTheme = ThemeData(
      colorScheme: ColorScheme(
        brightness: Brightness.light,
        primary: AppColors.white,
        onPrimary: AppColors.white.withOpacity(0.8),
        secondary: Colors.blue,
        onSecondary: Colors.blue.withOpacity(0.8),
        error: AppColors.red,
        onError: AppColors.red.withOpacity(0.8),
        background: AppColors.background,
        onBackground: AppColors.background.withOpacity(0.8),
        surface: AppColors.blue,
        onSurface: AppColors.blue.withOpacity(0.8),
      ),
      scaffoldBackgroundColor: AppColors.background,
      appBarTheme: const AppBarTheme(
          elevation: 0,
          centerTitle: true,
          color: AppColors.white,
          titleTextStyle: TextStyle(
              color: Colors.black,
              fontSize: 15,
              fontWeight: FontWeight.w500,
              fontFamily: manrope)),
      textTheme: const TextTheme(
        titleLarge: TextStyle(
            fontSize: 24, fontFamily: manrope, fontWeight: FontWeight.w600),
        bodyMedium: TextStyle(
            fontSize: 16, fontFamily: manrope, fontWeight: FontWeight.w400),
        titleMedium: TextStyle(
            fontSize: 16,
            fontFamily: manrope,
            fontWeight: FontWeight.w700,
            color: AppColors.white),
      ));
}
