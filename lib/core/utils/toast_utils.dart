import 'package:flutter/material.dart';

class ToastUtils {
  static void showShortToast(BuildContext context, String message) {
    dismissToast(context);
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.black.withOpacity(0.9),
        duration: const Duration(seconds: 5),
        content: Row(
          children: [
            Flexible(
              child: Text(
                message,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }

  static void dismissToast(BuildContext context) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
  }
}
