import 'package:dio/dio.dart';
import 'package:test_codeunion/core/constants/api.dart';
import 'package:test_codeunion/core/network_utils/dio_error.dart';

class DioClient {
  static Dio dio = Dio(BaseOptions(
      baseUrl: API.URL,
      connectTimeout: const Duration(milliseconds: 20000000),
      receiveTimeout: const Duration(milliseconds: 20000000),
      headers: {'Content-Type': 'application/json; charset=utf-8'},
      responseType: ResponseType.json))
    ..interceptors.addAll([CustomInterceptor(), logInterceptor]);

  static final logInterceptor = LogInterceptor(
    request: true,
    responseBody: true,
    requestBody: true,
    requestHeader: true,
  );

  static void setAccessToken(String? token) {
    if (token == null) {
      dio.options.headers.remove('Authorization');
    } else {
      dio.options.headers['Authorization'] = 'Bearer $token';
    }
  }

  static Future<Response> getRequest(String url) async => await dio.get(url);

  static Future<Response> postRequest(String url, {required data}) async =>
      await dio.post(url, data: data);

  static Future<Response> putRequest(String url, {data}) async =>
      await dio.put(url, data: data);

  static Future<Response> deleteRequest(String url) async =>
      await dio.delete(url);

  static Future uploadRequest(String url, String filePath) async {
    dio.options.contentType = "multipart/form-data";
    final multiPartFile = await MultipartFile.fromFile(
      filePath,
      filename: filePath.split('/').last,
    );
    FormData formData = FormData.fromMap({
      "file": multiPartFile,
    });
    final response = await dio.post(
      url,
      data: formData,
    );

    return response.data;
  }
}
