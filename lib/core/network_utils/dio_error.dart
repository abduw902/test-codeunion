import 'package:dio/dio.dart';

class CustomInterceptor extends Interceptor {
  @override
  Future onError(DioException err, ErrorInterceptorHandler handler) async {
    if (err.type == DioExceptionType.connectionError ||
        err.type == DioExceptionType.sendTimeout ||
        err.type == DioExceptionType.receiveTimeout) {
      return handler.next(err);
    } else {
      if (err.response?.statusCode == 422) {
        return handler.resolve(err.response!);
      }
    }
    return handler.next(err);
  }
}
