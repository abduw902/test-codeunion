import 'package:dio/io.dart';
import 'package:test_codeunion/core/network_utils/dio_client.dart';

///DIO CLIENT SOME EDITIONS TO FIX HANDSHAKING ERROR.
///THIS METHOD SHOULD BE CALLED AFTER JUST DIO INSTANCED

void dioClientAdapterFixHandshaking() {
  (DioClient.dio.httpClientAdapter as IOHttpClientAdapter).validateCertificate;
}
