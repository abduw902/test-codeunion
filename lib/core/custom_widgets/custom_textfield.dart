import 'package:flutter/material.dart';
import 'package:test_codeunion/core/theme/app_colors.dart';
import 'package:test_codeunion/core/theme/theme_style.dart';

class CustomTextField extends StatelessWidget {
  final TextInputAction? textInputAction;
  final Widget? suffixIcon;
  final String? initialValue;
  final bool autoFocus;
  final bool readOnly;
  final String? hintText;
  final int? maxLines;
  final TextStyle? hintStyle;
  final String? errorText;
  final TextInputType? keyboardType;
  final TextEditingController? controller;
  final FormFieldValidator<String>? validator;
  final Function(String)? getValue;
  final bool obscureText;

  const CustomTextField({
    Key? key,
    this.autoFocus = false,
    this.readOnly = false,
    this.initialValue,
    this.textInputAction,
    this.suffixIcon,
    this.hintText,
    this.hintStyle,
    this.errorText,
    this.obscureText = false,
    this.keyboardType,
    this.controller,
    this.validator,
    this.maxLines,
    this.getValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: validator,
      initialValue: initialValue,
      controller: controller,
      keyboardType: keyboardType,
      textInputAction: textInputAction,
      style: AppTheme.defaultTheme.textTheme.bodyMedium,
      cursorColor: AppColors.black,
      obscureText: obscureText,
      onChanged: (value) {
        if (getValue != null) getValue!(value);
      },
      autofocus: autoFocus,
      readOnly: readOnly,
      decoration: InputDecoration(
          border: InputBorder.none,
          hintText: hintText,
          hintStyle: AppTheme.defaultTheme.textTheme.bodyMedium!
              .copyWith(color: AppColors.grey)),
    );
  }
}
