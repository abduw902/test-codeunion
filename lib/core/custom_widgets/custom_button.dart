import 'package:flutter/material.dart';
import 'package:test_codeunion/core/theme/app_colors.dart';
import 'package:test_codeunion/core/theme/theme_style.dart';

class CustomButton extends StatelessWidget {
  final String title;
  final VoidCallback onTap;

  const CustomButton({super.key, required this.title, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 20),
          width: double.maxFinite,
          height: 64,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6), color: AppColors.blue),
          child: Center(
            child: Text(
              title,
              style: AppTheme.defaultTheme.textTheme.titleMedium,
            ),
          ),
        ),
      ),
    );
  }
}
