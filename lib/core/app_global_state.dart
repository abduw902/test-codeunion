import 'package:hive_flutter/adapters.dart';
import 'package:test_codeunion/core/constants/storage_constants.dart';
import 'package:test_codeunion/core/data/data_sourses/local/local_storage.dart';
import 'package:test_codeunion/pages/auth/data/models/user_model.dart';

class AppGlobalState {
  static late LocalStorage localStorage;

  static Future<void> init() async {
    await Hive.initFlutter();
    localStorage = LocalStorageImplements();

    if (!Hive.isAdapterRegistered(0)) {
      Hive.registerAdapter(UserModelAdapter());
    }

    await Hive.openBox(StorageConstants.userBox);
  }
}
