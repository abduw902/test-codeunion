import 'package:hive_flutter/hive_flutter.dart';
import 'package:test_codeunion/core/constants/storage_constants.dart';
import 'package:test_codeunion/pages/auth/data/models/user_model.dart';

abstract class LocalStorage {
  UserModel? getUserModel();

  void saveUserModel(UserModel userModel);

  Future<void> clearData();
}

class LocalStorageImplements implements LocalStorage {
  @override
  UserModel? getUserModel() {
    final box = Hive.box(StorageConstants.userBox);
    return box.get(StorageConstants.userBox, defaultValue: null);
  }

  @override
  void saveUserModel(UserModel userModel) {
    final box = Hive.box(StorageConstants.userBox);
    box.put(StorageConstants.userBox, userModel);
  }

  @override
  Future<void> clearData() async {
    final box = Hive.box(StorageConstants.userBox);
    await box.clear();
  }
}
