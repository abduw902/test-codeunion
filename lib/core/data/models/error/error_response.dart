class ErrorResponse {
  String? name;
  String? message;
  int? status;
  List<dynamic>? errors;

  ErrorResponse({
    this.name,
    this.message,
    this.status,
    this.errors,
  });

  factory ErrorResponse.fromJson(Map<String, dynamic> json) => ErrorResponse(
        name: json["name"],
        message: json["message"],
        status: json["status"],
        errors: json["errors"] == null
            ? []
            : List<dynamic>.from(json["errors"]!.map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "message": message,
        "status": status,
        "errors":
            errors == null ? [] : List<dynamic>.from(errors!.map((x) => x)),
      };
}
