import 'package:flutter/material.dart';
import 'package:test_codeunion/core/app_global_state.dart';
import 'package:test_codeunion/core/theme/app_colors.dart';
import 'package:test_codeunion/core/theme/theme_style.dart';
import 'package:test_codeunion/pages/auth/presentation/sign_in.dart';
import 'package:test_codeunion/pages/home/presentation/home.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
          future: Future.delayed(const Duration(seconds: 1)),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (AppGlobalState.localStorage.getUserModel() == null) {
                return const SignIn();
              } else {
                return const Home();
              }
            }
            return Stack(
              children: [
                Container(
                  color: AppColors.background,
                ),
                Center(
                    child: Text(
                  "Codeunion",
                  style: AppTheme.defaultTheme.textTheme.titleLarge,
                )),
              ],
            );
          }),
    );
  }
}
