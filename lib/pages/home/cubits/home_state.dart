part of 'home_cubit.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class HomeInitial extends HomeState {
  @override
  List<Object> get props => [];
}

class HomeIndexChanged extends HomeState {
  final int index;
  final String title;

  const HomeIndexChanged({required this.index, required this.title});

  @override
  List<Object?> get props => [index, title];
}
