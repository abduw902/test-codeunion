import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeInitial());

  final List<String> titles = ['Лента', 'Карта', 'Избранные', 'Профиль'];

  void changeIndex(int index) {
    emit(HomeIndexChanged(index: index, title: titles[index]));
  }
}
