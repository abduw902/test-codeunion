import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:test_codeunion/core/app_global_state.dart';
import 'package:test_codeunion/core/theme/app_colors.dart';
import 'package:test_codeunion/core/theme/theme_style.dart';
import 'package:test_codeunion/pages/auth/cubits/auth_cubit.dart';
import 'package:test_codeunion/pages/auth/presentation/sign_in.dart';

class Profile extends StatelessWidget {
  const Profile({super.key});

  @override
  Widget build(BuildContext context) {
    final user = AppGlobalState.localStorage.getUserModel()!;
    return Column(
      children: [
        const SizedBox(
          height: 40,
        ),
        Center(
          child: SvgPicture.asset(
            "assets/icons/profile.svg",
            height: 64,
            colorFilter:
                const ColorFilter.mode(AppColors.black, BlendMode.srcIn),
          ),
        ),
        const SizedBox(
          height: 15,
        ),
        Text(
          user.nickname,
          style: AppTheme.defaultTheme.textTheme.titleLarge,
        ),
        const SizedBox(
          height: 12,
        ),
        Text(
          user.email,
          style: AppTheme.defaultTheme.textTheme.bodyMedium!
              .copyWith(color: AppColors.grey),
        ),
        const SizedBox(
          height: 22,
        ),
        Container(
          width: double.maxFinite,
          padding: const EdgeInsets.symmetric(vertical: 22, horizontal: 30),
          color: AppColors.white,
          child: GestureDetector(
            onTap: () {
              BlocProvider.of<AuthCubit>(context).logout();
              Navigator.of(context).pushAndRemoveUntil(
                  CupertinoPageRoute(builder: (context) => const SignIn()),
                  (route) => false);
            },
            child: Text(
              "Выйти",
              style: AppTheme.defaultTheme.textTheme.bodyMedium!
                  .copyWith(color: AppColors.red),
            ),
          ),
        )
      ],
    );
  }
}
