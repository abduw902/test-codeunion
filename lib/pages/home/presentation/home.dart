import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:test_codeunion/core/theme/app_colors.dart';
import 'package:test_codeunion/pages/home/cubits/home_cubit.dart';
import 'package:test_codeunion/pages/home/presentation/tabs/favourites.dart';
import 'package:test_codeunion/pages/home/presentation/tabs/map.dart';
import 'package:test_codeunion/pages/home/presentation/tabs/newsline.dart';
import 'package:test_codeunion/pages/home/presentation/tabs/profile.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String title = 'Профиль';
    int index = 3;
    return BlocBuilder<HomeCubit, HomeState>(
      builder: (context, state) {
        if (state is HomeIndexChanged) {
          title = state.title;
          index = state.index;
        }
        return Scaffold(
          appBar: AppBar(
            title: Text(title),
          ),
          body: IndexedStack(
            index: index,
            children: const [
              NewsLine(),
              Maps(),
              Favourites(),
              Profile(),
            ],
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            currentIndex: index,
            onTap: (index) =>
                BlocProvider.of<HomeCubit>(context).changeIndex(index),
            showSelectedLabels: true,
            showUnselectedLabels: true,
            selectedFontSize: 10,
            unselectedFontSize: 10,
            selectedItemColor: AppColors.blue,
            unselectedItemColor: AppColors.black,
            items: [
              _bottomNavigationBarItem(
                  icon: "assets/icons/newsline.svg", label: "Лента"),
              _bottomNavigationBarItem(
                  icon: "assets/icons/map.svg", label: "Карта"),
              _bottomNavigationBarItem(
                  icon: "assets/icons/favourites.svg", label: "Избранные"),
              _bottomNavigationBarItem(
                  icon: "assets/icons/profile.svg", label: "Профиль"),
            ],
          ),
        );
      },
    );
  }

  BottomNavigationBarItem _bottomNavigationBarItem(
      {required String icon, required String label}) {
    return BottomNavigationBarItem(
      icon: SvgPicture.asset(
        icon,
        colorFilter: const ColorFilter.mode(AppColors.black, BlendMode.srcIn),
      ),
      activeIcon: SvgPicture.asset(
        icon,
        colorFilter: const ColorFilter.mode(AppColors.blue, BlendMode.srcIn),
      ),
      label: label,
    );
  }
}
