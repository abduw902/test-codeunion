import 'package:dartz/dartz.dart';
import 'package:test_codeunion/core/app_global_state.dart';
import 'package:test_codeunion/core/constants/api.dart';
import 'package:test_codeunion/core/data/models/error/error_response.dart';
import 'package:test_codeunion/core/network_utils/dio_client.dart';
import 'package:test_codeunion/pages/auth/data/models/login_response.dart';

abstract class AuthRepository {
  Future<Either<ErrorResponse, LoginResponse>> login(
      String email, String password);

  Future<void> logout();
}

class AuthRepositoryImpl implements AuthRepository {
  @override
  Future<Either<ErrorResponse, LoginResponse>> login(
      String email, String password) async {
    try {
      final res = await DioClient.postRequest('${API.URL}/api/v1/auth/login',
          data: {"email": email, "password": password});
      if (res.statusCode == 422) {
        return Left(ErrorResponse.fromJson(res.data));
      }
      return Right(LoginResponse.fromJson(res.data));
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<void> logout() async {
    DioClient.setAccessToken;
    await AppGlobalState.localStorage.clearData();
  }
}
