import 'package:test_codeunion/pages/auth/data/models/user_model.dart';

class LoginResponse {
  Tokens? tokens;
  UserModel? user;

  LoginResponse({
    this.tokens,
    this.user,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
        tokens: json["tokens"] == null ? null : Tokens.fromJson(json["tokens"]),
        user: json["user"] == null ? null : UserModel.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "tokens": tokens?.toJson(),
        "user": user?.toJson(),
      };
}

class Tokens {
  String? accessToken;
  String? refreshToken;

  Tokens({
    this.accessToken,
    this.refreshToken,
  });

  factory Tokens.fromJson(Map<String, dynamic> json) => Tokens(
        accessToken: json["accessToken"],
        refreshToken: json["refreshToken"],
      );

  Map<String, dynamic> toJson() => {
        "accessToken": accessToken,
        "refreshToken": refreshToken,
      };
}
