import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:test_codeunion/core/custom_widgets/custom_button.dart';
import 'package:test_codeunion/core/custom_widgets/keyboard_escape.dart';
import 'package:test_codeunion/core/theme/app_colors.dart';
import 'package:test_codeunion/core/utils/toast_utils.dart';
import 'package:test_codeunion/pages/auth/cubits/auth_cubit.dart';
import 'package:test_codeunion/pages/auth/presentation/login_and_pass_container.dart';
import 'package:test_codeunion/pages/home/presentation/home.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  late TextEditingController loginController;
  late TextEditingController passwordController;

  @override
  void initState() {
    loginController = TextEditingController();
    passwordController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardEscape(
      child: BlocListener<AuthCubit, AuthState>(
        listener: (context, state) {
          if (state is AuthLoading) {
            context.loaderOverlay.show();
          }
          if (state is AuthLoaded) {
            context.loaderOverlay.hide();
            Navigator.of(context).pushAndRemoveUntil(
                CupertinoPageRoute(builder: (context) => const Home()),
                (route) => false);
          }
          if (state is AuthError) {
            loginController.clear();
            passwordController.clear();
            ToastUtils.showShortToast(context, state.error);
            context.loaderOverlay.hide();
          }
        },
        child: Scaffold(
          appBar: AppBar(
            title: const Text("Авторизация"),
          ),
          body: LoaderOverlay(
            overlayColor: AppColors.background,
            overlayOpacity: 0.7,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                LoginAndPassContainer(
                  loginController: loginController,
                  passwordController: passwordController,
                ),
                const SizedBox(
                  height: 32,
                ),
                CustomButton(
                    title: "Войти",
                    onTap: () {
                      if (BlocProvider.of<AuthCubit>(context).isValid(
                          context, loginController, passwordController)) {
                        BlocProvider.of<AuthCubit>(context).login(
                            loginController.text, passwordController.text);
                      }
                    }),
                const SizedBox(
                  height: 20,
                ),
                CustomButton(title: "Зарегистрироваться", onTap: () {}),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    loginController.dispose();
    passwordController.dispose();
    super.dispose();
  }
}
