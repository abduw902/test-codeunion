import 'package:flutter/material.dart';
import 'package:test_codeunion/core/custom_widgets/custom_textfield.dart';
import 'package:test_codeunion/core/theme/app_colors.dart';

class LoginAndPassContainer extends StatelessWidget {
  final TextEditingController loginController;
  final TextEditingController passwordController;

  const LoginAndPassContainer({
    super.key,
    required this.loginController,
    required this.passwordController,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.white,
      padding: const EdgeInsets.all(16),
      child: Column(
        children: [
          CustomTextField(
            controller: loginController,
            getValue: (value) => loginController.text = value,
            hintText: "Логин или почта",
          ),
          const Divider(
            color: AppColors.grey,
          ),
          CustomTextField(
            controller: passwordController,
            getValue: (value) => passwordController.text = value,
            hintText: "Пароль",
            obscureText: true,
          ),
        ],
      ),
    );
  }
}
