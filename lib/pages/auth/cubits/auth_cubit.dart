import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_codeunion/core/app_global_state.dart';
import 'package:test_codeunion/core/network_utils/dio_client.dart';
import 'package:test_codeunion/core/utils/toast_utils.dart';
import 'package:test_codeunion/core/utils/validator_utils.dart';
import 'package:test_codeunion/pages/auth/data/models/user_model.dart';
import 'package:test_codeunion/pages/auth/data/repositories/auth_repository.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  final AuthRepository authRepository;

  AuthCubit(this.authRepository) : super(AuthInitial());

  bool isValid(context, loginController, passwordController) {
    if (loginController.text.isEmpty || passwordController.text.isEmpty) {
      loginController.clear();
      passwordController.clear();
      ToastUtils.showShortToast(context, "Введите логин и пароль");
      return false;
    }
    if (Validator().isValidEmail(loginController.text)) {
      if (Validator().isValidPassword(passwordController.text)) {
        return true;
      } else {
        passwordController.clear();
        ToastUtils.showShortToast(
            context, 'password length must be at least 8 characters long');
        return false;
      }
    } else {
      loginController.clear();
      passwordController.clear();
      ToastUtils.showShortToast(context, "email must be a valid email");
      return false;
    }
  }

  Future<void> login(String email, String password) async {
    emit(const AuthLoading());
    try {
      final res = await authRepository.login(email, password);
      res.fold(
        (error) => emit(
          AuthError(error.message.toString()),
        ),
        (success) {
          DioClient.setAccessToken(success.tokens!.accessToken);
          AppGlobalState.localStorage.saveUserModel(success.user!);
          emit(AuthLoaded(success.user!));
        },
      );
    } catch (e) {
      emit(AuthError(e.toString()));
    }
  }

  Future<void> logout() async {
    await authRepository.logout();
    emit(LogoutSuccess());
  }
}
