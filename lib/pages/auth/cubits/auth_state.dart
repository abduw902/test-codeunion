part of '../../../pages/auth/cubits/auth_cubit.dart';

abstract class AuthState extends Equatable {
  const AuthState();
}

class AuthInitial extends AuthState {
  @override
  List<Object> get props => [];
}

class AuthLoading extends AuthState {
  const AuthLoading();

  @override
  List<Object?> get props => [];
}

class AuthLoaded extends AuthState {
  final UserModel userModel;

  const AuthLoaded(this.userModel);

  @override
  List<Object?> get props => [userModel];
}

class AuthError extends AuthState {
  final String error;

  const AuthError(this.error);

  @override
  List<Object?> get props => [error];
}

class LogoutSuccess extends AuthState {
  @override
  List<Object?> get props => [];
}
