import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_codeunion/core/app_global_state.dart';
import 'package:test_codeunion/core/network_utils/handshaking_fix.dart';
import 'package:test_codeunion/core/theme/theme_style.dart';
import 'package:test_codeunion/dp.dart';

import 'pages/splash_screen/splash_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppGlobalState.init();
  dioClientAdapterFixHandshaking();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return DependencyProvider(
      child: MaterialApp(
        title: 'Codeunion',
        theme: AppTheme.defaultTheme,
        home: const SplashScreen(),
      ),
    );
  }
}
