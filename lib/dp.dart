import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_codeunion/pages/auth/cubits/auth_cubit.dart';
import 'package:test_codeunion/pages/auth/data/repositories/auth_repository.dart';
import 'package:test_codeunion/pages/home/cubits/home_cubit.dart';

class DependencyProvider extends StatelessWidget {
  final Widget child;

  const DependencyProvider({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (BuildContext context) => AuthCubit(
            AuthRepositoryImpl(),
          ),
        ),
        BlocProvider(
          create: (BuildContext context) => HomeCubit(),
        ),
      ],
      child: child,
    );
  }
}
